#!/bin/bash

set -o nounset -o errexit -o pipefail -o verbose

local curl_args=(
    "https://cloud.docker.com/api/build/v1/setting/${DOCKER_CLOUD_PROJECT_ID}/build/"
    -H 'Accept: application/json'
    -H 'Content-Type: application/json'
    --data '
        {
            "autobuild": true,
            "build_context": "/",
            "dockerfile": "Dockerfile",
            "nocache": false,
            "source_name": "master",
            "source_type": "Branch",
            "state": "Empty",
            "tag": "latest"
        }'
    --basic --user "${DOCKER_CLOUD_USERNAME}:${DOCKER_CLOUD_API_KEY}"
    --user-agent "curl/ci (repo: ${TRAVIS_REPO_SLUG}; job: ${TRAVIS_JOB_NUMBER}; event: ${TRAVIS_EVENT_TYPE})"
    --compressed
    # output verbose information about everything curl does and what it results to
    --verbose
    # do not output progressbar and errors
    --silent
    # show the errors, but keep the progressbar hidden
    --show-error
    --retry 3 --retry-delay 10 --retry-connrefused
    # trash the server response body only the headers matter
    --output /dev/null
)
curl "${curl_args[@]}" 2>&1 | sed -re '/^. [a-zA-Z-]*(cookie|auth)[a-zA-Z-]*: /I s,^(. [a-zA-Z-]*: ).*,\1<redacted>,I'
