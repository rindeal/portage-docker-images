#!/bin/bash

set -xvue

tmpdir="$(mktemp -d)"
echo "tmpdir=$tmpdir"
chmod -v --reference=/tmp "${tmpdir}"

cleanup() {
    rm -r "${tmpdir}"
}
trap cleanup EXIT INT TERM

EXT=$PWD/ext_storage

docker=(
  docker run
    -it
    -v $EXT/root/:/root:ro
    -v $EXT/etc--portage/:/etc/portage:ro
    -v $tmpdir:/tmp:rw
    --cap-add=SYS_PTRACE
    "${@}"
    rindeal/portage-amd64-base
)
"${docker[@]}"
