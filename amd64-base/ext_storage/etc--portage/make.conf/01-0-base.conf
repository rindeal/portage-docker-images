# ### USE flags and stuff ###

# reset
USE=""

# ipv6 is not widely supported on cloud machines
USE="${USE} -ipv6"
# yes, most cloud machines have more than one core
USE="${USE} threads"
USE="${USE} ssl"
# interactivity is limited
USE="${USE} -ncurses"
USE="${USE} readline"
USE="${USE} pcre"
# bindist openssl has issues connecting to various HTTPS sites
# ```
# error:14077410:SSL routines:SSL23_GET_SERVER_HELLO:sslv3
# ```
USE="${USE} -bindist"
USE="${USE} -xattr -acl -filecaps"
USE="${USE} -systemd -udev"
USE="${USE} pie"
USE="${USE} pam -cracklib"
USE="${USE} -berkdb"
USE="${USE} -openmp"

## no docs
USE="${USE} -doc -examples -handbook -man -gtk-doc"

## languages
USE="${USE} -vala -perl"

## i18n
USE="${USE} -nls unicode icu iconv"


# ### no multilib ###
ABI_X86="64"
MULTILIB_ABIS="amd64"
USE="${USE} -multilib"


# ### portage ###

# stuff acceptance
ACCEPT_KEYWORDS="amd64 ~amd64"
ACCEPT_LICENSE="*"

## features
# reset
FEATURES=""
FEATURES="${FEATURES} -cgroup"
FEATURES="${FEATURES} collision-protect config-protect-if-modified distlocks ebuild-locks"
FEATURES="${FEATURES} fixlafiles"
FEATURES="${FEATURES} -ipc-sandbox -network-sandbox"
FEATURES="${FEATURES} nodoc noinfo noman"
FEATURES="${FEATURES} notitles"
FEATURES="${FEATURES} parallel-fetch"
FEATURES="${FEATURES} preserve-libs"
FEATURES="${FEATURES} -sandbox"
FEATURES="${FEATURES} strict unknown-features-warn"
FEATURES="${FEATURES} userfetch userpriv usersandbox usersync"

## interactivity
ACCEPT_PROPERTIES="* -interactive"
CLEAN_DELAY="0"
EPAUSE_IGNORE="true"
EMERGE_WARNING_DELAY="0"

## emerge
EMERGE_DEFAULT_OPTS="--ask=n --verbose=y --verbose-conflicts --tree --color=y"
EMERGE_DEFAULT_OPTS="${EMERGE_DEFAULT_OPTS}"

# probably the only mirror with globaly distributed anycast-ed servers with HTTPS and IPv6
GENTOO_MIRRORS="https://mirrors.evowise.com/gentoo/"

# disable ecompress as docs and man pages are deleted
PORTAGE_COMPRESS=""

## dirs
PORTAGE_WORKDIR_MODE="0775"
# packages will be built in `/tmp/portage/*`
PORTAGE_TMPDIR="/tmp"
# put it here so that it can be cleaned up with one big sweep
DISTDIR="/tmp/portage/distfiles"
# PORTDIR shouldn't be used
PORTDIR="/var/portage/repos/gentoo"

# ## logging ##
PORTAGE_LOGDIR="/tmp/portage/logs"
PORT_LOGDIR="${PORTAGE_LOGDIR}"
PORTAGE_ELOG_CLASSES=""
PORTAGE_ELOG_SYSTEM=""
EMERGE_LOG_DIR="${PORTAGE_LOGDIR}"

INSTALL_MASK="${INSTALL_MASK} *.html *.pdf"
INSTALL_MASK="${INSTALL_MASK} /etc/portage"
INSTALL_MASK="${INSTALL_MASK} /etc/logrotate.d"
INSTALL_MASK="${INSTALL_MASK} /usr/share/bash-completion/"
INSTALL_MASK="${INSTALL_MASK} /etc/init.d/ /etc/conf.d/ /lib/systemd/"
## useless python test(s) packages
INSTALL_MASK="${INSTALL_MASK} /usr/lib*/python*/test /usr/lib*/python*/tests"
INSTALL_MASK="${INSTALL_MASK} /usr/lib*/python*/*/test /usr/lib*/python*/*/tests"
INSTALL_MASK="${INSTALL_MASK} /usr/lib*/python*/*/*/test /usr/lib*/python*/*/*/tests"
## package-specific
# NOTE: do not remove the `*` wildcard from the templates dir as git check its existence and warns if not found
INSTALL_MASK="${INSTALL_MASK} /usr/share/git-core/templates/* /usr/share/git/contrib/"

# -----------------------------------------------------------------------------

CHOST="x86_64-pc-linux-gnu"

# ### CFLAGS ###
# reset
CFLAGS=""
# -O1 is by far the best when weighing in performance/size/speed
CFLAGS="${CFLAGS} -O1 -pipe"
# useless for our containers
CFLAGS="${CFLAGS} -fno-stack-protector -fomit-frame-pointer"
CFLAGS="${CFLAGS} -malign-data=abi"
# choosing sandybridge as no cloud should ever use anything older than this
CFLAGS="${CFLAGS} -mmmx -msse -msse2 -msse3 -mssse3 -mcx16 -msahf -maes -mpclmul -mpopcnt -mavx -msse4.2 -msse4.1 -mfxsr -mxsave -mxsaveopt"

CPU_FLAGS_X86="aes avx mmx mmxext popcnt sse sse2 sse3 sse4_1 sse4_2 ssse3"


# ### CXXFLAGS ###
# reset
CXXFLAGS=""
CXXFLAGS="${CFLAGS}"

MAKEOPTS="-j 4"

L10N=""

PYTHON_TARGETS="python3_6"
PYTHON_SINGLE_TARGET="python3_6"
